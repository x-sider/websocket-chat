const http = require('http');
const fs = require('fs');
const WebSocketServer = require('websocket').server;
const readline = require('readline');

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

const httpServer = http.createServer((req, res) => {
	let file = (fName, fType) => {
		res.writeHead(200, { 'Content-Type': 'text/' + fType});
		fs.readFile(fName, function (err, data) {
			if (err) res.end('Error: ' + err);
			else res.end(data);
		});
	};
	switch(req.url) {
		case '/':
			file('index.html', 'html');
			break;
		default:
			res.writeHead(404, { 'Content-Type': 'text/plain' });
			res.end('404 не найдено');
	}

});
const websocketServer = http.createServer(function(request, response) {});


httpServer.listen(8080, function() {
	console.log((new Date()) + ' HTTP Server is running');
});

websocketServer.listen(8081, function() {
	console.log((new Date()) + ' WebSocket Server is listening on port 8081');
});

const websocketListener = new WebSocketServer({
	httpServer: websocketServer
});

let connections = [];

websocketListener.on('request', function(request) {
	const connection = request.accept(null, request.origin);
	connections.push(connection);
	sendAll({
		'type': 'count',
		'value': connections.length
	});
	connection.on('message', function(message) {
		try {
			let data = JSON.parse(message.utf8Data);

			switch (data.type) {
				case 'message':
					if (data.name) {
						sendAll(data)
					}
					break;
			}
		} catch (e) {
			// silent
		}
	});
	connection.on('close', function(reasonCode, description) {
		connections = connections.filter((c) => {
			return c !== connection;
		});
		sendAll({
			'type': 'count',
			'value': connections.length
		})
	});
});

function sendAll(ob) {
	connections.forEach((c) => {
		c.sendUTF(JSON.stringify(ob));
	});
}

function ask() {
	rl.question('Сообщение: ', (answer) => {
		sendAll({
			type: 'message',
			name: 'Администрация портала localhost',
			value: answer
		});
		ask();
	});
}

ask();